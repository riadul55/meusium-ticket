class Environments {
  static const String PROD = 'prod';
  static const String DEV = 'dev';
  static const String LOCAL = 'local';
}

class ConfigEnvironments {
  static const String _currentEnvironments = Environments.LOCAL;
  static const List<Map<String, String>> _availableEnvironments = [
    {
      'env': Environments.PROD,
      'url': '',
    },
    {
      'env': Environments.DEV,
      'url': '',
    },
    {
      'env': Environments.LOCAL,
      'url': '',
    },
  ];

  static Map<String, String> getEnvironments() {
    return _availableEnvironments.firstWhere(
          (d) => d['env'] == _currentEnvironments,
    );
  }
}