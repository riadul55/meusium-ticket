import 'package:flutter/material.dart';

import 'app/modules/main_app.dart';
import 'app/routes/app_pages.dart';
import 'initializer.dart';

void main() async {
  await Initializer.init();
  String route = await AppPages.INITIAL;
  runApp(MainApp(initialRoute: route));
}
