import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:museum_ticket/app/values/themes/app_themes.dart';

import '../routes/app_pages.dart';

class MainApp extends StatelessWidget {
  final String initialRoute;
  const MainApp({Key? key, required this.initialRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Museum Ticket Booking",
      theme: AppThemes.lightThemeData,
      darkTheme: AppThemes.lightThemeData,
      initialRoute: initialRoute,
      getPages: AppPages.routes,
    );
  }
}
