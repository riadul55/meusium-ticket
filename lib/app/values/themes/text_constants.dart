class TextConstants {
  static const xsTextSize = 10.0;
  static const xsTextWeight = 600;
  static const xsTextLineHeight = 18;

  static const sTextSize = 14.0;
  static const sTextWeight = 400;
  static const sTextLineHeight = 20;

  static const mTextSize = 16.0;
  static const mTextWeight = 500;
  static const mTextLineHeight = 24;

  static const lTextSize = 18.0;
  static const lTextWeight = 500;
  static const lTextLineHeight = 32;

  static const xlTextSize = 24.0;
  static const xlTextWeight = 600;
  static const xlTextLineHeight = 32;

  static const xBTextSize = 32.0;
  static const xBTextWeight = 600;
  static const xBTextLineHeight = 43.2;
}