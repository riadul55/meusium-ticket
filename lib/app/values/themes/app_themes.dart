import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import '../colors/app_colors.dart';

class AppThemes {
  static final Color _lightFocusColor = Colors.black.withOpacity(0.12);

  static ThemeData lightThemeData = themeData(lightColorScheme, _lightFocusColor);

  static ThemeData themeData(ColorScheme colorScheme, Color focusColor) {
    return ThemeData(
      textTheme: GoogleFonts.poppinsTextTheme(),
      fontFamily: "Roboto",
      colorScheme: colorScheme,
      appBarTheme: AppBarTheme(
        color: colorScheme.onPrimaryContainer,
        backgroundColor: colorScheme.onPrimaryContainer,
        iconTheme: IconThemeData(color: colorScheme.primaryContainer),
        elevation: 2,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: colorScheme.primary,
          statusBarBrightness: colorScheme.brightness,
        )
      ),
      iconTheme: IconThemeData(color: colorScheme.onPrimaryContainer),
      canvasColor: colorScheme.background,
      scaffoldBackgroundColor: colorScheme.background,
      highlightColor: Colors.transparent,
      focusColor: focusColor,
    );
  }

  static final ColorScheme lightColorScheme = ColorScheme(
    primary: AppColors.primary,
    onPrimary: AppColors.onPrimary,

    primaryContainer: AppColors.primaryContainer,
    onPrimaryContainer: AppColors.onPrimaryContainer,

    secondary: AppColors.secondary,
    onSecondary: AppColors.onSecondary,

    secondaryContainer: AppColors.secondaryContainer,
    onSecondaryContainer: AppColors.onSecondaryContainer,

    background: AppColors.background,
    onBackground: AppColors.onBackground,

    surface: AppColors.surface,
    onSurface: AppColors.onSurface,

    error: AppColors.error,
    onError: AppColors.onError,

    outline: AppColors.outline,

    brightness: Brightness.light,

  );
}