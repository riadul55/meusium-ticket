class AuthRequestBody {
  String email;
  String password;

  AuthRequestBody({required String email, required String password})
      : email = email.trim(),
        password = password.trim();

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['email'] = email;
    data['password'] = password;
    return data;
  }
}
