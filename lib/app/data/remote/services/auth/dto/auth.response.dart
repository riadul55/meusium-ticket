class AuthResponse {
/*
{
  "_token": "23|VUu1hdMQjU7sul2kZl1MONKFZx7pdnfPPQ4zZmIo",
    "error": "not found"
}
*/

  String? token;
  String? error;

  AuthResponse({
    this.token,
    this.error,
  });
  AuthResponse.fromJson(Map<String, dynamic> json) {
    token = json['_token']?.toString();
    error = json['error']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_token'] = token;
    data['error'] = token;
    return data;
  }
}