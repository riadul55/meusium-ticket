
import 'package:get/get.dart';

import 'dto/auth.request.body.dart';
import 'dto/auth.response.dart';

class AuthService {
  final GetConnect _connect;

  const AuthService(GetConnect connect) : _connect = connect;

  String get _prefix => 'api';

  Future<AuthResponse> authenticateUser(AuthRequestBody body) async {
    return await _connect.post("$_prefix/login", body.toJson()).then(
          (response) => response.statusCode == 200
          ? AuthResponse.fromJson(response.body)
          : throw (response.body),
    );
  }
}
