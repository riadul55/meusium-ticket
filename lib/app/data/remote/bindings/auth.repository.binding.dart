import 'package:get/get.dart';

import '../repositories/auth.repository.dart';
import '../services/auth/auth.service.dart';

class AuthRepositoryBinding {
  late AuthRepository _authRepository;

  AuthRepository get repository => _authRepository;

  AuthRepositoryBinding() {
    final getConnect = Get.find<GetConnect>();
    final authService = AuthService(getConnect);
    _authRepository = AuthRepository(authService: authService);
  }
}