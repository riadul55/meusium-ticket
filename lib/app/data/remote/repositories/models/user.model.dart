
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../services/auth/dto/auth.response.dart';

class UserModel {
  final String email, token;

  const UserModel({
    required this.email,
    required this.token,
  });

  factory UserModel.fromData(String email,  AuthResponse data) {
    return UserModel(email: email, token: data.token!);
  }

  static UserModel? fromStorage() {
    final storage = Get.find<GetStorage>();
    final user = storage.read<UserModel>('user');
    return user;
  }

  Future<void> save() async {
    final storage = Get.find<GetStorage>();
    await storage.write('user', {'email': email, 'token': token});
  }
}