import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../services/auth/auth.service.dart';
import '../services/auth/dto/auth.request.body.dart';
import 'models/user.model.dart';

class AuthRepository {
  final AuthService _authService;
  final _storage = Get.find<GetStorage>();

  AuthRepository({required AuthService authService})
      : _authService = authService;

  Future<UserModel> authenticalUser({
    required String username,
    required String password,
  }) async {
    try {
      final body = AuthRequestBody(email: username, password: password);
      final response = await _authService.authenticateUser(body);

      final user = UserModel.fromData(username, response);
      await user.save();

      return user;
    } catch (e) {
      return throw (e);
    }
  }

  Future<bool> isAuthenticated() {
    return Future.value(false);
  }
}