import 'package:get/get.dart';

import '../data/remote/bindings/auth.repository.binding.dart';
import '../modules/auth/bindings/auth_binding.dart';
import '../modules/auth/views/auth_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static Future<String> get INITIAL async {
    try {
      final authDomainService = AuthRepositoryBinding().repository;
      final authenticated = await authDomainService.isAuthenticated();
      return !authenticated ? Routes.AUTH : Routes.HOME;
    } catch (err) {
      return Routes.AUTH;
    }
  }

  static final routes = [
    GetPage(
      name: _Paths.AUTH,
      page: () => const AuthView(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
  ];
}
